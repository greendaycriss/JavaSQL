
- CD into "JavaSQL" directory:  
`cd JavaSQL`


- Compile source code into "bin" directory:  
`javac -d bin src/Main.java`


- Run compiled code with jar dependency:  
`java -cp "bin;lib/postgresql-42.2.19.jar" Main`
