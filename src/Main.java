import java.sql.*;


class Main {
    
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.postgresql.Driver";  
    static final String DB_URL = "jdbc:postgresql://localhost:5432/students";
    
    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "postgres";
    
    
    public static void createDatabase() throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", USER, PASS);
        String query = "create database STUDENTS";
                        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(query);
    }
    
    public static void createTable() throws SQLException
    {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
                             
        String query = "CREATE TABLE REGISTRATION " +
                        "(id INTEGER not NULL, " +
                        " first VARCHAR(255), " + 
                        " last VARCHAR(255), " + 
                        " age INTEGER, " + 
                        " PRIMARY KEY ( id ))";
                        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(query);
    }
    
    public static void queryDatabase() throws SQLException {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASS);

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT id, first, last, age FROM REGISTRATION");
        
        while (rs.next()) {
            int id = rs.getInt("id");
            String first = rs.getString("first");
            String last = rs.getString("last");
            int age = rs.getInt("age");
            System.out.println(id + ", " + first + ", " + last + ", " + age);
        }
    }
    
    public static void insertRow() throws SQLException {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASS);

        Statement stmt = con.createStatement();
        String query = "INSERT INTO REGISTRATION " +
                       "VALUES(2, 'Valy', 'Dan', 9)";
        
        stmt.executeUpdate(query);
    }
    

    public static void main(String args[]) {        
        try {
            
            Class.forName(JDBC_DRIVER);
            
            createDatabase();

            createTable();
            
            insertRow();
            
            queryDatabase();
            
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

}

